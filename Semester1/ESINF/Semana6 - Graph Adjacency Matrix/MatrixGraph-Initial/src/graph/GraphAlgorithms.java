package graph;

import graphexamples.Highway;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Iterator;

/**
 * Implementation of graph algorithms for a (undirected) graph structure
 * Considering generic vertex V and edge E types
 *
 * Works on AdjancyMatrixGraph objects
 *
 * @author DEI-ESINF
 *
 */
public class GraphAlgorithms {

    private static <T> LinkedList<T> reverse(LinkedList<T> list) {
        LinkedList<T> reversed = new LinkedList<T>();
        Iterator<T> it = list.iterator();
        while (it.hasNext()) {
            reversed.push(it.next());
        }
        return reversed;
    }

    /**
     * Performs depth-first search of the graph starting at vertex. Calls
     * package recursive version of the method.
     *
     * @param graph Graph object
     * @param vertex Vertex of graph that will be the source of the search
     * @return queue of vertices found by search (including vertex), null if
     * vertex does not exist
     *
     */
    public static <V, E> LinkedList<V> DFS(AdjacencyMatrixGraph<V, E> graph, V vertex) {

        int index = graph.toIndex(vertex);
        if (index == -1) {
            return null;
        }

        LinkedList<V> resultQueue = new LinkedList<V>();
        resultQueue.add(vertex);
        boolean[] knownVertices = new boolean[graph.numVertices];
        DFS(graph, index, knownVertices, resultQueue);
        return resultQueue;
    }

    /**
     * Actual depth-first search of the graph starting at vertex. The method
     * adds discovered vertices (including vertex) to the queue of vertices
     *
     * @param graph Graph object
     * @param index Index of vertex of graph that will be the source of the
     * search
     * @param known previously discovered vertices
     * @param verticesQueue queue of vertices found by search
     *
     */
    static <V, E> void DFS(AdjacencyMatrixGraph<V, E> graph, int index, boolean[] knownVertices,
            LinkedList<V> verticesQueue) {
        knownVertices[index] = true;
        for (int i = 0; i < graph.numVertices; i++) {
            if (graph.edgeMatrix[index][i] != null && knownVertices[i] == false) {
                verticesQueue.add(graph.vertices.get(i));
                DFS(graph, i, knownVertices, verticesQueue);
            }
        }
    }

    /**
     * Performs breath-first search of the graph starting at vertex. The method
     * adds discovered vertices (including vertex) to the queue of vertices
     *
     * @param graph Graph object
     * @param vertex Vertex of graph that will be the source of the search
     * @return queue of vertices found by search (including vertex), null if
     * vertex does not exist
     *
     */
    public static <V, E> LinkedList<V> BFS(AdjacencyMatrixGraph<V, E> graph, V vertex) {
        if (!graph.checkVertex(vertex)) {
            return null;
        }
        LinkedList<V> queueBFS = new LinkedList();
        LinkedList<V> queueAux = new LinkedList();
        boolean[] visited = new boolean[graph.numVertices];

        queueBFS.add(vertex);
        queueAux.add(vertex);
        visited[graph.toIndex(vertex)] = true;
        while (!queueAux.isEmpty()) {
            V src = queueAux.removeFirst();
            for (V adj : graph.directConnections(src)) {
                if (!visited[graph.toIndex(adj)]) {
                    queueBFS.add(adj);
                    queueAux.add(adj);
                    visited[graph.toIndex(adj)] = true;
                }
            }
        }

        return queueBFS;
    }

    /**
     * All paths between two vertices Calls recursive version of the method.
     *
     * @param graph Graph object
     * @param source Source vertex of path
     * @param dest Destination vertex of path
     * @param path LinkedList with paths (queues)
     * @return false if vertices not in the graph
     *
     */
    public static <V, E> boolean allPaths(AdjacencyMatrixGraph<V, E> graph,
            V source,
            V dest,
            LinkedList<LinkedList<V>> paths) {

        if (!graph.checkVertex(source) || !graph.checkVertex(dest)) {
            return false;
        }

        paths.clear();
        boolean[] knownVertices = new boolean[graph.numVertices];
        LinkedList<V> auxStack = new LinkedList();
        allPaths(graph, graph.toIndex(source), graph.toIndex(dest), knownVertices, auxStack, paths);
        return true;
    }

    /**
     * Actual paths search The method adds vertices to the current path (stack
     * of vertices) when destination is found, the current path is saved to the
     * list of paths
     *
     * @param graph Graph object
     * @param sourceIdx Index of source vertex
     * @param destIdx Index of destination vertex
     * @param knownVertices previously discovered vertices
     * @param auxStack stack of vertices in the path
     * @param path LinkedList with paths (queues)
     *
     */
    static <V, E> void allPaths(AdjacencyMatrixGraph<V, E> graph,
            int sourceIdx,
            int destIdx,
            boolean[] knownVertices,
            LinkedList<V> auxStack,
            LinkedList<LinkedList<V>> paths) {

        auxStack.push(graph.vertices.get(sourceIdx));
        knownVertices[sourceIdx] = true;
        for (int i = 0; i < graph.numVertices; i++) {

            if (graph.edgeMatrix[sourceIdx][i] != null) {
                if (i == destIdx) {
                    auxStack.push(graph.vertices.get(i));
                    paths.add(reverse(auxStack));
                    auxStack.pop();
                } else if (!knownVertices[i]) {
                    allPaths(graph, i, destIdx, knownVertices, auxStack, paths);
                }
            }
        }
        knownVertices[sourceIdx] = false;
        auxStack.pop();

    }

    /**
     * Transforms a graph into its transitive closure uses the Floyd-Warshall
     * algorithm
     *
     * @param graph Graph object
     * @param dummyEdge object to insert in the newly created edges
     * @return the new graph
     */
    public static <V, E> AdjacencyMatrixGraph<V, E> transitiveClosure(AdjacencyMatrixGraph<V, E> graph, E dummyEdge) {
        AdjacencyMatrixGraph<V, E> graphCopy = (AdjacencyMatrixGraph<V, E>) graph.clone();

        // tmb da pa ver distancia minima
        // acrescenta ramos diretos de ligaçoes transitivas
        for (int k = 0; k < graph.numVertices; k++) {
            for (int i = 0; i < graph.numVertices; i++) {
                if (i != k && graph.privateGet(i, k) != null) {
                    for (int j = 0; j < graph.numVertices; j++) {
                        if (i != j && k != j && graph.privateGet(k, j) != null) {
                            graphCopy.privateSet(i, j, dummyEdge);
                        }
                    }
                }
            }
        }
        return graphCopy;
    }

    public static <V, E> boolean existsPath(AdjacencyMatrixGraph<V, E> graph,
            V source,
            V dest,
            LinkedList<V> path) {

        if (!graph.checkVertex(source) || !graph.checkVertex(dest)) {
            return false;
        }

        path.clear();

        LinkedList<V> auxStack = new LinkedList();
        boolean[] knownVertices = new boolean[graph.numVertices];

        auxStack.push(graph.vertices.get(graph.toIndex(source)));

        if (existsPath(graph, graph.toIndex(source), graph.toIndex(dest),
                knownVertices, auxStack)) {
            path.addAll(reverse(auxStack));
            return true;
        }
        return false;
    }

    static <V, E> boolean existsPath(AdjacencyMatrixGraph<V, E> graph,
            int sourceIdx,
            int destIdx,
            boolean[] knownVertices,
            LinkedList<V> auxStack) {

        if (sourceIdx == destIdx) {

            return true;
        }
        knownVertices[sourceIdx] = true;
        for (int i = 0; i < graph.numVertices; i++) {
            if (graph.edgeMatrix[sourceIdx][i] != null && knownVertices[i] == false) {
                auxStack.push(graph.vertices.get(i));
                if (existsPath(graph, i, destIdx, knownVertices, auxStack)) {
                    return true;
                }
                auxStack.pop();
            }
        }
        return false;
    }



}
