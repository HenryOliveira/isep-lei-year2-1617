package graphexamples;

import graph.AdjacencyMatrixGraph;
import graph.GraphAlgorithms;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * A class to represent a labyrinth map with rooms, doors and exits Methods
 * discover the nearest exit and the path to it Stores a graph of private Room
 * vertex and Door edges
 *
 * @author DEI-ESINF
 *
 */
public class LabyrinthCheater {

    private class Room {

        String name;
        public boolean hasExit;
        List<Door> ramos;

        public Room(String name, boolean hasExit) {
            ramos = new ArrayList();
            this.name = name;
            this.hasExit = hasExit;
        }

        @Override
        public boolean equals(Object obj) {
            if (obj == this) {
                return true;
            }

            if (obj == null || obj.getClass() != getClass()) {
                return false;
            }

            Room that = (Room) obj;
            return this.name.equals(that.name);
        }

    }

    private class Door {
    }

    AdjacencyMatrixGraph<Room, Door> map;

    public LabyrinthCheater() {
        map = new AdjacencyMatrixGraph<Room, Door>();
    }

    /**
     * Inserts a new room in the map
     *
     * @param city
     * @return false if city exists in the map
     */
    public boolean insertRoom(String name, boolean hasExit) {
        return map.insertVertex(new Room(name, hasExit));
    }

    /**
     * Inserts a new door in the map fails if room does not exist or door
     * already exists
     *
     * @param from room
     * @param to room
     * @return false if a room does not exist or door already exists between
     * rooms
     */
    public boolean insertDoor(String from, String to) {
        return map.insertEdge(new Room(from, false), new Room(to, false), new Door());
    }

    /**
     * Returns a list of rooms which are reachable from one room
     *
     * @param room
     * @return list of room names or null if room does not exist
     */
    public Iterable<String> roomsInReach(String room) {
        if (!map.checkVertex(new Room(room, false))) {
            return null;
        }

        LinkedList<Room> rooms;
        rooms = GraphAlgorithms.DFS(map, new Room(room, false));

        LinkedList<String> names = new LinkedList();
        for (Room r : rooms) {
            names.add(r.name);
        }

        return names;
    }

    /**
     * Returns the nearest room with an exit
     *
     * @param room from room
     * @return room name or null if from room does not exist or there is no
     * reachable exit
     */
    public String nearestExit(String room) {
        if (!map.checkVertex(new Room(room, false))) {
            return null;
        }
        LinkedList<Room> rooms;
        rooms = GraphAlgorithms.BFS(map, new Room(room, false));

        for (Room r : rooms) {
            if (r.hasExit) {
                return r.name;
            }
        }

        return null;
    }

    /**
     * Returns the shortest path to the nearest room with an exit
     *
     * @param room from room
     * @return list of room names or null if from room does not exist or there
     * is no reachable exit
     */
    public LinkedList<String> pathToExit(String from) {
        if (!map.checkVertex(new Room(from, false))) {
            return null;
        }
        LinkedList<LinkedList<Room>> paths = new LinkedList();

        String exitName = nearestExit(from);
        if (exitName == null) {
            return null;
        }

        Room exit = new Room(exitName, true);
        boolean result = GraphAlgorithms.allPaths(map, new Room(from, false), exit, paths);
        if (result == false) {
            return null;
        }
        if (paths.isEmpty()) {
            return null;
        }

        Iterator<LinkedList<Room>> it = paths.listIterator();
        LinkedList<Room> min = it.next();
        while (it.hasNext()) {

            LinkedList<Room> cur = it.next();
            if (cur.size() < min.size()) {
                min = cur;
            }
        }

        LinkedList<String> names = new LinkedList();
        for (Room r : min) {
            names.add(r.name);

        }

        return names;
    }

}
