/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sortcomplexity;

import java.util.Random;

/**
 *
 * @author Henrique Oliveira [1150738@isep.ipp.pt]
 */
public class SortComplexity {

    
    public static void main(String[] args){
        System.out.println("Test com 20:\n");
        test(20);
        System.out.println("\n\n");
        System.out.println("Test com 1000:\n");
        test(1000);
        System.out.println("\n\n");
        System.out.println("Test com 91000:\n");
        test(91000);
        System.out.println("\n\n");
    }
    /**
     * @param args the command line arguments
     */
    public static void test(int size) {
        int max = size + 5000;
        int[] arrayO = new int[size];
        int loop = 0;

        Random generator = new Random();
        generator.nextInt(max); //generating one

        for (int i = 0; i < size; i++) {
            arrayO[i] = generator.nextInt(max);
        }


        int[] array = arrayO.clone();
        
        long result;

        Quicksort q = new Quicksort();
        long startTime = System.currentTimeMillis();
        q.sort(array);        
        long endTime = System.currentTimeMillis();
        result = endTime - startTime;

        System.out.println("The quick sort runtime is " + result + " miliseconds");

        long result2;
        array = arrayO.clone();
        long startTime2 = System.currentTimeMillis();
        InsertionSort.insertion_srt(array, size);
        long endTime2 = System.currentTimeMillis();
        result2 = endTime2 - startTime2;
        System.out.println("The insertion sort is " + result2 + " miliseconds");

        long result3;
        array = arrayO.clone();
        long startTime3 = System.currentTimeMillis();
        BubbleSort.sort(array, size);
        long endTime3 = System.currentTimeMillis();
        result3 = endTime3 - startTime3;
        System.out.println("The bubble sort runtime is " + result3 + " miliseconds");

        long result4;
        array = arrayO.clone();
        Mergesort m = new Mergesort();
        long startTime4 = System.currentTimeMillis();
        m.sort(array); 
        long endTime4 = System.currentTimeMillis();
        result4 = endTime4 - startTime4;
        System.out.println("The merge sort runtime is " + result4 + " miliseconds");

    }

}
