/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sortcomplexity;

import java.util.Scanner;

/**
 *
 * @author Henrique Oliveira [1150738@isep.ipp.pt]
 */
public class BubbleSort {

    public static void sort(int[]array, int n) {
        int c, d, swap;

        for (c = 0; c < (n - 1); c++) {
            for (d = 0; d < n - c - 1; d++) {
                if (array[d] > array[d + 1]) /* For descending order use < */ {
                    swap = array[d];
                    array[d] = array[d + 1];
                    array[d + 1] = swap;
                }
            }
        }
    }
}
