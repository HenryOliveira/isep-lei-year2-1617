
package graphexamples;

import graphbase.Edge;
import graphbase.Graph;
import graphbase.Vertex;
import static graphbase.GraphAlgorithms.*;
import java.util.ArrayList;
import java.util.LinkedList;
 

/**
 *
 * DEI-ESINF
 */

public class Museum { 
    
    private Graph<Room, Integer> exhibition;
    
    public Museum(){
        exhibition = new Graph<Room, Integer>(false);
    }
    
    public void addtwoConnectedRooms(Integer n1, Double t1, Integer n2, Double t2){
        exhibition.insertEdge(new Room(n1, t1), new Room(n2, t2), null, 0);
    }
    
    public double timevisitAllrooms(){
        double tottime = 0;
        for(Room room: exhibition.vertices()){
            tottime += room.getTime();
        }
        return tottime;
    }
    
    
    
    
    //------------ Static nested Room class ------------
    
    public static class Room implements Comparable {
       
        private Integer number;
        private Double time;
        
        public Room (Integer n, Double t) {number=n; time=t;}
        
        public Integer getNumber() {return number;}
        public void setNumber(Integer n) {number=n;}
        
        Double getTime() {return time;}
        public void setTime(Double t) {time=t;}
        
        public int compareTo(Object otherObject) {
        
            Room otherRoom = (Room) otherObject ;
            if (this.time < otherRoom.time)  return -1;
            if (this.time == otherRoom.time) return 0;
            return 1;
        }
        
        @Override
        public boolean equals(Object otherObj) {
            if (this == otherObj){
                return true;
            }
            if (otherObj == null || this.getClass() != otherObj.getClass()){
                return false;
            }
            Room otherRoom = (Room) otherObj;
        
            return this.number == otherRoom.number;
        }
        
        @Override
        public String toString() {
            String st="";
            st+= "Room: "+number+" (Time: "+time+")" ;
            return st;
    } 
    
    }
    //------------ end of Static nested Room class ------------
    
    
}


