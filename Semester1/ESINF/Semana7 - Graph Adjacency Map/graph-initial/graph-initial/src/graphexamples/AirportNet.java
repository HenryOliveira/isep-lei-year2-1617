package graphexamples;

import graphbase.Edge;
import graphbase.Graph;
import static graphbase.GraphAlgorithms.DepthFirstSearch;
import java.util.Queue;

/**
 *
 * @author DEI-ESINF
 */

public class AirportNet {
    
    private Graph<String, Integer> graph;
    
    public AirportNet(){
        graph = new Graph(true);
    }
    
    public int getAirportKey(String airport){
        return graph.getKey(airport);
    }
    
    public String getAirportByKey(int key){
        String[] keysairport = graph.allkeyVerts();
        return keysairport[key];
    }
    
    public Integer getTraffic(String airportFrom, String airportTo){
        if(!graph.validVertex(airportTo) || !graph.validVertex(airportFrom)){
            return null;
        }
        
        Edge<String, Integer> edge1 = graph.getEdge(airportTo, airportFrom);
        Edge<String, Integer> edge2 = graph.getEdge(airportFrom, airportTo);
        
        if(edge1 == null || edge2 == null){
            return null;
        }
        
        return edge1.getElement() + edge2.getElement();
    }
    
    public Double getMiles(String airportFrom, String airportTo){
        Edge<String, Integer> edge = graph.getEdge(airportFrom, airportTo);
        if(edge == null){
            return null;
        }
        
        return edge.getWeight();
    }
    
    public String getRoutesAirport(){
        String routesAirp = "";
        
        for(String airp: graph.vertices()){
            int grau = graph.outDegree(airp) + graph.inDegree(airp);
            routesAirp += airp + " " + grau+"\n";
        }
        return routesAirp;
        
    }
}
