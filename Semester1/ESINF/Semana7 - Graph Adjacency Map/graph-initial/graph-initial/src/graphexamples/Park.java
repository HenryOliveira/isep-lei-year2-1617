
package graphexamples;

import graphbase.Graph;
import graphbase.GraphAlgorithms;
import java.util.LinkedList;


/**
*  @author DEI-ESINF
*/

public class Park {
    
    public class Activity implements Comparable {
       
        private String code;
        private Double time;
        
        public Activity (String c, Double t) {code=c; time=t;}
        
        public String getCode() {return code;}
        public void setNumber(String c) {code=c;}
        
        Double getTime() {return time;}
        public void setTime(Double t) {time=t;}
        
        @Override
        public boolean equals(Object otherObj) {
            if (this == otherObj){
                return true;
            }
            if (otherObj == null || this.getClass() != otherObj.getClass()){
                return false;
            }
            Activity otherActiv = (Activity) otherObj;
        
            return this.code == otherActiv.code;
        }
        
        public int compareTo(Object otherObject) {
        
            Activity otherActiv = (Activity) otherObject ;
            if (this.time < otherActiv.time)  return -1;
            if (this.time == otherActiv.time) return 0;
            return 1;
        }
        
        @Override
        public String toString() {
            String st="";
            st+= code+" (Time: "+time+")" ;
            return st;
        }  
    }
    //------------ end of Static nested Room class ------------
    
    
}