/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.time.LocalDate;
import java.util.*;

/**
 *
 * @author DEI-ISEP
 */
public class Supermarket {

    Map<Invoice, Set<Product>> m;

    Supermarket() {
        m = new HashMap<>();
    }

    // Reads invoices from a list of String
    void getInvoices(List<String> l) throws Exception {
////        int line = 0;
////        String content;
////         String[] tokens;
////         while (line < l.size()) {
//// ////             content = l.get(line);
////             if (content.split(",")[0].equals("I")) {
////                 tokens = content.split(",");
//// 
////                 Invoice i = new Invoice(tokens[1], tokens[2]);
////                 Set<Product> s = new HashSet();
//// 
////                 content = l.get(++line);
////                 while (line < l.size() && content.split(",")[0].equals("P")) {
////                     tokens = content.split(",");
////                     Product p = new Product(tokens[1], Integer.parseInt(tokens[2]), Long.parseLong(tokens[3]));
//// 
////                     s.add(p);
////                     ++line;
////                     if (line < l.size()) {
////                         content = l.get(line);
////                     }
////                 }
//// 
////                 m.put(i, s);
////             } else {
////                 line++;
////             }
////         }
        Invoice i = null;
        Product p = null;
        Set<Product> sp = null;

        for (String line : l) {
            String[] tokens = line.split(",");
            switch (tokens[0]) {
                case "I":
                    i = new Invoice(tokens[1], tokens[2]);
                    sp = new HashSet();
                    m.put(i, sp);
                    break;

                case "P":
                    p = new Product(tokens[1], Integer.parseInt(tokens[2]), Long.parseLong(tokens[3]));
                    sp.add(p);
                    break;
            }
        }

    }

    // returns a set in which each number is the number of products in the r
    // invoice 
    Map<Invoice, Integer> numberOfProductsPerInvoice() {
        Map<Invoice, Integer> count = new HashMap();

        for (Map.Entry<Invoice, Set<Product>> entry : m.entrySet()) {
            Invoice i = entry.getKey();
            int c = entry.getValue().size();
            count.put(i, c);
        }
        return count;
    }

    // returns a Set of invoices in which each date is >d1 and <d2
    Set<Invoice> betweenDates(LocalDate d1, LocalDate d2) {
        Set<Invoice> set = new HashSet();

        for (Map.Entry<Invoice, Set<Product>> entry : m.entrySet()) {
            Invoice i = entry.getKey();
            if (i.getDate().isAfter(d1) && i.getDate().isBefore(d2)) {
                set.add(i);
            }
        }

        return set;
    }

    // returns the sum of the price of the product in all the invoices
    long totalOfProduct(String productId) {

        long total = 0;
        for (Map.Entry<Invoice, Set<Product>> entry : m.entrySet()) {
            Set<Product> s = entry.getValue();
            for (Product p : s) {
                if (p.getIdentification().equalsIgnoreCase(productId)) {
                    total += p.getPrice() * p.getQuantity();
                }
            }
        }
        return total;
    }

    // converts a map of invoices and troducts to a map which key is a product 
    // identification and the values are a set of the invoice references 
    // in which it appearss
    Map<String, Set<Invoice>> convertInvoices() {
        Map<String, Set<Invoice>> map = new HashMap();

        for (Map.Entry<Invoice, Set<Product>> entry : m.entrySet()) {
            Invoice i = entry.getKey();
            Set<Product> s = entry.getValue();
            for (Product p : s) {
                String id = p.getIdentification();
                if(map.containsKey(id)){
                    Set<Invoice> set = map.get(id);
                    set.add(i);
                } else {
                    Set<Invoice> set = new HashSet();
                    set.add(i);
                    map.put(id, set);
                }                
            }
        }
        return map;
    }
}
