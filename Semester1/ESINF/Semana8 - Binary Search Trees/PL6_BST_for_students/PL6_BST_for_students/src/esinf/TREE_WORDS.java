package esinf;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author DEI-ESINF
 */
public class TREE_WORDS extends BST<TextWord> {

    public void createTree() throws FileNotFoundException {
        Scanner ler = new Scanner(new File("src/esinf/xxx.xxx"));
        while (ler.hasNextLine()) {
            String line = ler.next();
            String[] tokens = line.split(" |,|.|\n");
            for (String tok : tokens) {
                insert(new TextWord(tok, 0));
            }
        }
        ler.close();
    }
//================================================================ 

    /**
     * Inserts a new word in the tree, or increments the number of its
     * occurrences.
     *
     * @param element
     */
    @Override
    public void insert(TextWord element) {
        root = insert(element, root);
    }

    private Node<TextWord> insert(TextWord element, Node<TextWord> node) {
        if (node == null) {
            return new Node(element, null, null);
        }

        if (element.compareTo(node.getElement()) < 0) {
            node.setLeft(insert(element, node.getLeft()));
        } else if (element.compareTo(node.getElement()) > 0) {
            node.setRight(insert(element, node.getRight()));
        } else {
            node.getElement().incOcorrences();
        }

        return node;
    }
//****************************************************************

    /**
     * Returns a map with a list of words for each occurrence found.
     *
     * @return a map with a list of words for each occurrence found.
     */
    public Map<Integer, List<String>> getWordsOccurrences() {
        Map<Integer, List<String>> map = new HashMap();

        Iterable<TextWord> elements = this.inOrder();
        
        for(TextWord w:elements){
            if(!map.containsKey(w.getOcorrences())){
                map.put(w.getOcorrences(), new LinkedList());
            }
            map.get(w.getOcorrences()).add(w.getWord());
        }
        
        return map;
    }

}
