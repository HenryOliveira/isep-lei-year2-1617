/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_pl1_semana1;

/**
 *
 * @author Henrique Oliveira [1150738@isep.ipp.pt]
 */
public class Ex1 {

    public static String m(String s0) {
        String s1 = s0;
        return s1;
    }

    public static String copyString(String s0) {
        if (s0 == null) {
            return null;
        }

        if (s0.length() == 1) {
            return s0;
        } else {
            return s0.substring(0, 1) + copyString(s0.substring(1));
        }
    }

    public static String copyStringReverse(String s0) { //Backtracking Bitches
        if (s0 == null) {
            return null;
        }

        if (s0.length() == 1) {
            return s0;
        } else {
            return copyStringReverse(s0.substring(1)) + s0.substring(0, 1);
        }
    }

}
