/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_pl1_semana1;

/**
 *
 * @author Henrique Oliveira [1150738@isep.ipp.pt]
 */
public class Ex2 {
    
    public static int invertNum(int num, int power){
        if(num < 10)
            return num;
        else{

            int res = num % 10;
            return res * (int)Math.pow(10, power) + invertNum(num/10, --power);
        }
    }
    
    public static int invertNum2(int num1, int num2){
        if(num1 == 0)
            return num2;
        else {
            num2 = num2*10;
            num2 = num2 + (num1%10);
            num1 = num1/10;
            return invertNum2(num1, num2);
        }
    }
}
