/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_pl1_semana1;

/**
 *
 * @author Henrique Oliveira [1150738@isep.ipp.pt]
 */
public class Ex3 {
    
    
    public static int mdc(int m, int n){
        if(n < m)
            return mdc(n, m);
        if(n >=m && n % m == 0)
            return m;
        else
            return mdc(m, n%m);
    }
    
}
