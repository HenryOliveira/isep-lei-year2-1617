/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_pl1_semana1;

/**
 *
 * @author Henrique Oliveira [1150738@isep.ipp.pt]
 */
public class ESINF_PL1_Semana1 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //System.out.println(Ex1.copyString("Henrique"));
        //System.out.println(Ex1.copyStringReverse("Henrique"));
        //System.out.println(Ex2.invertNum(123456, 5));
        
        //System.out.println(Ex3.mdc(48, 30));
        
        System.out.format("Labirinto Resolvido? (V2): %s%n", Ex4.solveMazeV2(Ex4.MAZE1, 0, 0, 6, 12));
        Ex4.printMaze(Ex4.MAZE1);
    }

}
