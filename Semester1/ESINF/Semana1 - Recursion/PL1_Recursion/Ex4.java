/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf_pl1_semana1;

/**
 *
 * @author Henrique Oliveira [1150738@isep.ipp.pt]
 */
public class Ex4 {

    public static int MAZE1[][]
            = {
                {1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1},
                {1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1},
                {1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1},
                {1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1},
                {1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0},
                {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0},
                {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1}
            };

//    public static void solveMazeProfessor(int[][] maze, int y, int x) {
//        if (x < 0 || x > 12 || y < 0 || y > 6) {
//            return;
//        }
//
//        if (maze[y][x] == 1) {
//            maze[y][x] = 9;
//
//            if (y == 6 && x == 12) { //Solution Found //Base case
//                return;
//            }
//
//            solveMazeProfessor(maze, y - 1, x);
//            solveMazeProfessor(maze, y, x + 1);
//            solveMazeProfessor(maze, y + 1, x);
//            solveMazeProfessor(maze, y, x - 1);
//
//            if (maze[6][12] != 9) {
//                maze[y][x] = 2;
//            }
//        }
//    }

    public static boolean solveMazeV2(int[][] maze, int i, int j, int idest, int jdest) {
        if (i < 0 || i > idest || j < 0 || j > jdest) {
            return false;
        }

        if (maze[i][j] != 1) {
            return false;
        }

        maze[i][j] = 9;

        if (i == idest && j == jdest) {
            return true;
        }

        boolean res = solveMazeV2(maze, i - 1, j, idest, jdest); //tenta norte

        if (!res) {
            res = solveMazeV2(maze, i, j + 1, idest, jdest);//tenta este
        }

        if (!res) {
            res = solveMazeV2(maze, i + 1, j, idest, jdest);//tenta sul
        }

        if (!res) {
            res = solveMazeV2(maze, i, j - 1, idest, jdest);//tenta oeste
        }

        if (!res) {
            maze[i][j] = 2;
        }

        return res;
    }

    public static boolean solveMaze(int[][] maze, int i, int j, int idest, int jdest) {
        if (maze[i][j] == 0) {
            return false;
        }

        maze[i][j] = 9;
        boolean res = false;

        if (i == idest && j == jdest) {
            return true;
        } else {

            if (!res && canMove(maze, i, j, Direction.NORTH)) {
                res = solveMaze(maze, i - 1, j, idest, jdest);
            }
            if (!res && canMove(maze, i, j, Direction.EAST)) {
                res = solveMaze(maze, i, j + 1, idest, jdest);
            }
            if (!res && canMove(maze, i, j, Direction.SOUTH)) {
                res = solveMaze(maze, i + 1, j, idest, jdest);
            }
            if (!res && canMove(maze, i, j, Direction.WEST)) {
                res = solveMaze(maze, i, j - 1, idest, jdest);
            }

            if (!res) {
                maze[i][j] = 2;
            }
            return res;
        }
    }

    public static enum Direction {
        NORTH, EAST, SOUTH, WEST
    };

    public static boolean canMove(int[][] maze, int i, int j, Direction d) {;
        switch (d) {
            case NORTH:
                if (i == 0) {
                    return false;
                } else {
                    return maze[i - 1][j] == 1;
                }
            case EAST:
                if (j == maze[i].length - 1) {
                    return false;
                } else {
                    return maze[i][j + 1] == 1;
                }
            case SOUTH:
                if (i == maze.length - 1) {
                    return false;
                } else {
                    return maze[i + 1][j] == 1;
                }

            case WEST:
                if (j == 0) {
                    return false;
                } else {
                    return maze[i][j - 1] == 1;
                }

            default:
                return false;
        }
    }

    public static void printMaze(int[][] maze) {
        System.out.println();

        for (int[] maze1 : maze) {
            for (int j = 0; j < maze1.length; j++) {
                String prefix = "";
                String sufix = "";
                if (maze1[j] == 2) {
                    prefix = ANSI_BLUE;
                    sufix = ANSI_RESET;
                }
                if (maze1[j] == 9) {
                    prefix = ANSI_GREEN;
                    sufix = ANSI_RESET;
                }
                if (maze1[j] == 1) {
                    prefix = ANSI_YELLOW;
                    sufix = ANSI_RESET;
                }
                System.out.format(prefix + " %d " + sufix, maze1[j]);
                if (j != maze1.length - 1) {
                    System.out.print("|");
                } else {
                    System.out.println();
                }
            }
        }
        System.out.println();
    }

    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

}
