#include <stdio.h>
#include <limits.h>

int main(void)
{
	printf("Size of char in bits: %d\n", CHAR_BIT);
	printf("Size of int in bits: %d\n", sizeof(int) * CHAR_BIT);
	printf("Size of long int in bits: %d\n", sizeof(long int) * CHAR_BIT);
	printf("Size of float in bits: %d\n", sizeof(float) * CHAR_BIT);
	printf("Size of double in bits: %d\n", sizeof(double) * CHAR_BIT);
	return 0;
}
