#include <stdio.h>
#include <string.h>

int string_to_int(char *str)
{
	int str_len = strlen(str); //length of string

	int num=0, i;
	for(i = 0; i < str_len; i++)
	{
		int digit = str[i] - '0'; // '0' = 48
		num = num*10 + digit;
	}

	return num;
}


int main(void)
{
	printf(" \"123456\" to number: %d\n",string_to_int("123456"));
	return 0;
}
